# -*- coding: utf-8 -*-
"""User views."""
from flask import Blueprint, render_template, redirect, flash
from flask_login import login_required
from .forms import FeatureRequestForm
from .models import FeatureRequest
from bcore.database import db

blueprint = Blueprint('user', __name__, url_prefix='/users', static_folder='../static')


@blueprint.route('/', methods=['GET', 'POST'])
@login_required
def members():
    """List members."""
    form = FeatureRequestForm()
    if form.validate_on_submit():
        feature_request = FeatureRequest(title=form.title,
                                        description=form.description,
                                        client=form.client,
                                        client_priority=form.client_priority,
                                        target_date=form.target_date,
                                        product_area=form.product_area)
        db.session.add(feature_request)
        db.session.commit()
        flash('Form Submitted Successfully!')
    return render_template('users/features.html', form=form)
