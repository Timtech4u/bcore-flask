# -*- coding: utf-8 -*-
"""User forms."""
from flask_wtf import FlaskForm
from wtforms import ValidationError, PasswordField, StringField, SubmitField, TextField, IntegerField, SelectField, DateField
from wtforms.validators import InputRequired, Email, EqualTo, Length

from .models import User, FeatureRequest

class FeatureRequestForm(FlaskForm):
    """Feature Request Form."""

    title = StringField('Title', validators=[InputRequired(), Length(min=3, max=80)])
    description = TextField('Description', validators=[InputRequired(), Length(min=10, max=500)])
    client = SelectField('Client', choices=[('Client A', 'Client A'), ('Client B', 'Client B'), ('Client C', 'Client C')], validators=[InputRequired()])
    client_priority = IntegerField('Client Priority', validators=[InputRequired()])
    target_date = DateField('Target Date', validators=[InputRequired()])
    product_area = SelectField('Product Area', choices=[('Policies', 'Policies'), ('Billing', 'Billing'), ('Claims', 'Claims'), ('Reports', 'Reports')], validators=[InputRequired()])
    submit = SubmitField('Create Feature Request')

    def __init__(self, *args, **kwargs):
        """Create instance."""
        super(FeatureRequestForm, self).__init__(*args, **kwargs)

    def validate(self):
        """Validate the form."""
        initial_validation = super(FeatureRequestForm, self).validate()
        if not initial_validation:
            return False
        
        fq = FeatureRequest.query.filter_by(client_priority=self.client_priority).first()
        if fq:
            raise ValidationError("Priority number is already taken for another client")
            return False

 
class RegisterForm(FlaskForm):
    """Register form."""

    username = StringField('Username',
                           validators=[InputRequired(), Length(min=3, max=25)])
    email = StringField('Email',
                        validators=[InputRequired(), Email(), Length(min=6, max=40)])
    password = PasswordField('Password',
                             validators=[InputRequired(), Length(min=6, max=40)])
    confirm = PasswordField('Verify password',
                            [InputRequired(), EqualTo('password', message='Passwords must match')])

    def __init__(self, *args, **kwargs):
        """Create instance."""
        super(RegisterForm, self).__init__(*args, **kwargs)
        self.user = None

    def validate(self):
        """Validate the form."""
        initial_validation = super(RegisterForm, self).validate()
        if not initial_validation:
            return False
        user = User.query.filter_by(username=self.username.data).first()
        if user:
            self.username.errors.append('Username already registered')
            return False
        user = User.query.filter_by(email=self.email.data).first()
        if user:
            self.email.errors.append('Email already registered')
            return False
        return True
